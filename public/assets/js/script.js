let numberA = null;
let numberB = null;
let operation = null;

let inputDisplay = document.querySelector("#txt-input-display");

let btnNumbers = document.querySelectorAll('.btn-numbers');

let btnAdd = document.querySelector('#btn-add');
let btnSubtract = document.querySelector('#btn-subtract');
let btnMultiply = document.querySelector('#btn-multiply');
let btnDivide = document.querySelector('#btn-divide');
let btnEqual = document.querySelector('#btn-equal');
let btnDecimal = document.querySelector('#btn-decimal');
let btnClearAll = document.querySelector('#btn-clear-all');
let btnBackspace = document.querySelector('#btn-backspace');

let nameInputs = document.querySelectorAll('.name-input');

let fname = document.querySelector('#fname-input');
let lname = document.querySelector('#lname-input');
let nameDisplay = document.querySelector('#name-display');

btnNumbers.forEach((btnNumber) => {
	btnNumber.addEventListener('click', () => {
		inputDisplay.value += btnNumber.textContent;
	});
});

btnAdd.onclick = () => {
	if (numberA === null) {
		numberA = Number(inputDisplay.value);
		operation = "addition";
		inputDisplay.value = null;
	} else if (numberB === null) {
		console.log("B is null")
		numberB = Number(inputDisplay.value);
		numberA += numberB;
		operation = "addition";
		numberB = null;
		inputDisplay.value = null;
	}
}

btnSubtract.onclick = () => {
	if (numberA === null) {
		numberA = Number(inputDisplay.value);
		operation = "subtract";
		inputDisplay.value = null;
	} else if (numberB === null) {
		console.log("B is null")
		numberB = Number(inputDisplay.value);
		numberA -= numberB;
		operation = "subtract";
		numberB = null;
		inputDisplay.value = null;
	}
}

btnDivide.onclick = () => {
	if (numberA === null) {
		numberA = Number(inputDisplay.value);
		operation = "divide";
		inputDisplay.value = null;
	} else if (numberB === null) {
		console.log("B is null")
		numberB = Number(inputDisplay.value);
		numberA /= numberB;
		operation = "divide";
		numberB = null;
		inputDisplay.value = null;
	}
}

btnMultiply.onclick = () => {
	if (numberA === null) {
		numberA = Number(inputDisplay.value);
		operation = "multiply";
		inputDisplay.value = null;
	} else if (numberB === null) {
		console.log("B is null")
		numberB = Number(inputDisplay.value);
		numberA *= numberB;
		operation = "multiply";
		numberB = null;
		inputDisplay.value = null;
	}
}

btnEqual.onclick = () => {
	if (numberB === null && inputDisplay.value !== "") {
		numberB = inputDisplay.value;
	}

	if (operation === "addition") {
		inputDisplay.value = Number(numberA) + Number(numberB);
	}

	if (operation === "subtract") {
		inputDisplay.value = Number(numberA) - Number(numberB);
	}

	if (operation === "multiply") {
		inputDisplay.value = Number(numberA) * Number(numberB);
	}

	if (operation === "divide") {
		inputDisplay.value = Number(numberA) / Number(numberB);
	}
}

btnDecimal.onclick = () => {
	if (!inputDisplay.value.includes('.')) {
		inputDisplay.value = inputDisplay.value + btnDecimal.textContent;
	}
}

btnBackspace.onclick = () => {
	inputDisplay.value = inputDisplay.value.slice(0, inputDisplay.value.length - 1);
}

btnClearAll.onclick = () => {
	inputDisplay.value = null;
	numberA = null;
	numberB = null;
	operation = null;
}

nameInputs.forEach((nameInput) => {
	nameInput.addEventListener('input', () => {
		nameDisplay.innerHTML = `${fname.value} ${lname.value}`
	})
});

